import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import groovy.util.slurpersupport.GPathResult;
import groovy.xml.MarkupBuilder;
import java.beans.PropertyVetoException;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.codehaus.groovy.ast.stmt.CaseStatement

	Logger logger = Logger.getLogger("emagiz.as400pgm.groovy");
	logger.log(Level.FINEST, "Parsing payload as xml:payload = ${payload}");
	def pcNode = new XmlSlurper().parseText(payload);
	// Todo: Think about whether a namespace or not is required. Below snippet is an example of how to apply a namespace to your selection.
	//.declareNamespace('ns':'https://www.capegroep.nl/ns/as400/1.0/')
	
	assert pcNode instanceof groovy.util.slurpersupport.GPathResult : "Unable to parse input as XML.";
	assert 1 == pcNode.ProgramName.size() : "A ProgramName element is required for the node '${pcNode.name()}'. The ProgramName equals a fully qualified name of a AS400 program call in the following format: /QSYS.LIB/ANITEST.LIB/ANITEST.PGM";
	def childNodes = pcNode.'*'.findAll{ childNode ->
		childNode.name() != "ProgramName"
	}
	def outputParameterList = [];
	
	class OutputParameter {
		DataType type;
		ProgramParameter param;
		String nodeName;
		AS400DataType as400DataType;
	}
	
	AS400 as400 = null;
	ProgramParameter[] parmList;
	ProgramCall programCall;
		try {
			logger.log(Level.FINE,"Configuring connection with AS400");
			logger.log(Level.FINEST,"Using credentials Host: ${HOST} Username: ${UID} Password ${PWD}");
			
			// Create an AS400 object
			as400 = new AS400(HOST, UID, PWD);
			// Create a parameter list
			// The list must have both input and output parameters
			// Todo: Catch the case when no input and output parameters are required. Either disallow this by checking for atleast 1 childNode or skip paramlist part.
			parmList = new ProgramParameter[childNodes.size()];
			
			childNodes.eachWithIndex { node , index ->
				//General validations
				Direction direction;
				try {direction = node.GeneralAttributes.direction.text().toUpperCase()}
				finally {assert direction : "The direction '${node.GeneralAttributes.direction.text()}' of node '${node.name()}' is not part of ${Direction.values()}"}
				// Datatype specific validations and actions
				DataType dataType;
				try{dataType = node.GeneralAttributes.type.text().toUpperCase()}
				finally{
					assert dataType : "Provided type '${node.GeneralAttributes.type}' of node '${node.name()}' is not part of ${DataType.values()}";
				}
				ProgramParameter param;
				AS400DataType as400outputDataType;
				switch(dataType) {
						case DataType.PACKEDDECIMAL :
							def numberOfDigits = 1..31;
							assert node.PackedDecimalAttributes.numberOfDigits.text() ==~ /\d+/ && numberOfDigits.contains(node.PackedDecimalAttributes.numberOfDigits.toInteger()) : "Provided numberOfDigits '${node.PackedDecimalAttributes.numberOfDigits}' of node '${node.name()}' is not in the range of [${numberOfDigits.getFrom()+ '..'+numberOfDigits.getTo()}]";
							def numberOfDecimals = 0..node.PackedDecimalAttributes.numberOfDigits.toInteger();
							assert node.PackedDecimalAttributes.numberOfDecimals.text() ==~ /\d+/ && numberOfDecimals.contains(node.PackedDecimalAttributes.numberOfDecimals.toInteger()) : "Provided numberOfDecimals '${node.PackedDecimalAttributes.numberOfDecimals}' of node '${node.name()}' should be in the range of [0..${node.PackedDecimalAttributes.numberOfDigits}] (numberOfDigits)";
							
							as400outputDataType = new AS400PackedDecimal(node.PackedDecimalAttributes.numberOfDigits.toInteger(),node.PackedDecimalAttributes.numberOfDecimals.toInteger());
							param = new ProgramParameter(as400outputDataType.toBytes(new BigDecimal(node.GeneralAttributes.value.text() ?: "0")));
						break;
						
						case DataType.TEXT :
							assert node.TextAttributes.byteLength.text() ==~ /\d+/ : "Provided byteLength '${node.TextAttributes.byteLength}' of node '${node.name()}' is not a valid integer.";
							def ccsid;
							if (node.ccsid.size() == 1) {
								assert node.ccsid.text() ==~ /\d+/ : "Provided ccsid '${node.ccsid}' is not a valid integer";
								ccsid = node.ccsid.toInteger();
							}
							else
								ccsid = 65535;
							as400outputDataType= new AS400Text(node.TextAttributes.byteLength.toInteger(),ccsid)
							param =  node.GeneralAttributes.value.text() ? new ProgramParameter(as400outputDataType.toBytes(node.GeneralAttributes.value.text())) : new ProgramParameter(node.TextAttributes.byteLength.toInteger());
						break;
						default :
						assert false : "Provided type '${node.GeneralAttributes.type}' of node '${node.name()}' is not implemented yet or not part of ${DataType.values()}";
						break;
				}
				if (direction == direction.OUTPUT) {
					outputParameterList.push(new OutputParameter(nodeName: node.name(),param: param,type: dataType,as400DataType: as400outputDataType));}
				parmList[index] = param;
			}
			/**
			 * Create a program object specifying the name of the program and
			 * the parameter list.
			 */
			logger.log(Level.FINE,"Start program call with parameter list");
			programCall = new ProgramCall(as400);
			programCall.setProgram(pcNode.ProgramName.text(), parmList);
			
			// Create a writer to generate the response 
			def writer = new StringWriter();
			def xml = new MarkupBuilder(writer);
			
			// Run the program.
			if (!programCall.run()) {
				/**
				 * If the AS/400 is not run then look at the message list to
				 * find out why it didn't run.
				 */
				logger.log(Level.WARNING, "Could not run program, see the response message for the details.");
				AS400Message[] messageList = programCall.getMessageList();
				if (messageList.size() > 0) {
					xml.response(){
						for (AS400Message message : messageList) {
							Message(message.getText());
						}
					}
				}
				else {xml.response(){}}
				return writer.toString();
			} else {
				/**
				 * Else the program is successfull. Process the output, which
				 * contains the returned data.
				 */

				// Loop through all output nodes to build the xml response	
				
				xml.response() {
					outputParameterList.each{OutputParameter outputNode ->
					"${outputNode.nodeName}"(convertAS400BytesToString(outputNode.param.getOutputData(),outputNode.as400DataType))
					}
				}
				
				return writer.toString();
				
			}
 
		} finally {
				// Make sure to disconnect
				if (as400 != null) {
					as400.disconnectAllServices();
				}
		
		}
			
		//Response converters to generate xml
		String convertAS400BytesToString(byte[] outputData, AS400DataType as400Type) {
			if (outputData != null) {
			switch (as400Type.getInstanceType()) {
				case DataType.PACKEDDECIMAL.getDataValue() :
					return ((BigDecimal) as400Type.toObject(outputData)).toString();
				case DataType.TEXT.getDataValue() :
					return (String) as400Type.toObject(outputData);
				default :
					return null;
				}
		}
			else
				return null;
		}
		
		// Static values
		enum DataType{
			BIN2(1), BIN4(2), BIN8(3), BYTEARRAY(4), FLOAT4(5), FLOAT8(6), PACKEDDECIMAL(7), STRUCTURE(8), TEXT(9),UBIN2(10),UBIN4(11),ZONEDDECIMAL(12),DECIMALFLOAT(13),BIN1(14),UBIN1(15),UBIN8(16),DATE(17),TIME(18),TIMESTAMP(19)
			DataType(int value) {
				this.staticDataValue = value
			}
			private final int staticDataValue
			public int getDataValue() {
				staticDataValue
			}
			public String toString() {
				return name()
			}
		}
		
		enum Direction{
			INPUT,OUTPUT
		}
		

		
		