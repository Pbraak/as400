# AS400 Connector #

The AS400 connector is a groovy script that facilitates a translation of xml messages provided by an emagiz flow to an actual program call in the AS400 environment.

### What is this repository for? ###

* This repository is used to manage the code base of the as400 connector. It should not be downloaded directly from here in your message bus. Instead it should be downloaded from the eMagiz app store.
* Version 1.0
